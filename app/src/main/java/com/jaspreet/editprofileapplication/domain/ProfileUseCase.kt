package com.jaspreet.editprofileapplication.domain

import android.util.Log
import com.jaspreet.editprofileapplication.R
import com.jaspreet.editprofileapplication.repo.network.ApiService
import com.jaspreet.editprofileapplication.repo.network.model.profile.ProfileResponse
import com.jaspreet.editprofileapplication.util.EvaluationFailedException
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ProfileUseCase @Inject constructor (private val  apiService: ApiService ){

    fun getProfile(id:String) =
        apiService.getProfile(id).observeOn(Schedulers.io()).subscribeOn(Schedulers.io())

    fun updateProfile(
        id: String,
        displayName: String,
        realName: String,
        dob: String,
        genderID: String?,
        ethnicityID: String,
        religionID: String,
        figureID: String,
        maritalStatusID: String?,
        occupation: String,
        aboutMe: String,
        lat: String?,
        lon: String?,
        height:String
    ) : Single<ProfileResponse> {

      return  Single.fromCallable {

          if (id.isNullOrBlank())
                throw EvaluationFailedException(R.string.user_id_cannot_empty)
          if (ethnicityID.isNullOrBlank())
              throw EvaluationFailedException(R.string.ethnicity_cannot_be_empty)
          if (religionID.isNullOrBlank())
              throw EvaluationFailedException(R.string.religion_cannot_be_empty)
          if (figureID.isNullOrBlank())
              throw EvaluationFailedException(R.string.figure_cannot_be_empty)
          if (height.isNullOrBlank())
              throw EvaluationFailedException(R.string.height_cannot_be_empty)
          if (occupation.isNullOrBlank())
              throw EvaluationFailedException(R.string.occupation_cannot_be_empty)
          if (aboutMe.isNullOrBlank())
              throw EvaluationFailedException(R.string.about_me_cannot_be_empty)



            var profileBody = ProfileResponse(aboutMe,dob,displayName,ethnicityID,figureID,genderID,height,lat?:"",lon?:"",maritalStatusID,occupation,realName,religionID)

            return@fromCallable profileBody
        }
            .flatMap {
                Log.d("==profile body==","==pr $it")
                apiService.updateProfile(id, it)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
            }
    }
}