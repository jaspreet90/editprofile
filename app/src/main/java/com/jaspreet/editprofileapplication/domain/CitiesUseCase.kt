package com.jaspreet.editprofileapplication.domain

import android.util.Log
import com.google.gson.Gson
import com.jaspreet.editprofileapplication.readassets.ReadAsset
import com.jaspreet.editprofileapplication.repo.db.location.LocationInfo
import com.jaspreet.editprofileapplication.repo.db.location.LocationRepo
import com.jaspreet.editprofileapplication.repo.network.model.location.LocationReponse
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CitiesUseCase @Inject constructor (private val locationRepo: LocationRepo){

    @Inject
    lateinit var readAsset: ReadAsset


    fun storeLocationIntoDB(fileName:String)=
            readAsset
                .read(fileName)
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .map {
                    return@map mapToDB(it)
                }.subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .flatMapCompletable {
                    locationRepo.insert(it)
                }
                .subscribeOn(Schedulers.io())


    private fun mapToDB(it: String) : ArrayList<LocationInfo> {
        var res= Gson().fromJson(it,LocationReponse::class.java)

        var citiesList = ArrayList<LocationInfo>()

        for (city in res.cities){
            citiesList.add(
                LocationInfo(
                    cityName = city.city,
                    lat = city.lat, lon = city.lon
                )
            )
        }
        return citiesList
    }

    fun getCitiesFromDB(searchItem: String?) :Flowable<List<LocationInfo>> {

        return if (searchItem!=null) {
            locationRepo.getSearchedLocations("%$searchItem%").observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
        } else{
            Flowable.just(emptyList<LocationInfo>()).observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
        }
    }

    fun getAllCitiesFromDB() = locationRepo.getAllCities()
        .observeOn(Schedulers.io())
        .subscribeOn(Schedulers.io())
}