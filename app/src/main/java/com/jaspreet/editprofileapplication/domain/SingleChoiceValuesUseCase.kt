package com.jaspreet.editprofileapplication.domain


import android.content.SharedPreferences
import com.google.gson.Gson
import com.jaspreet.editprofileapplication.repo.network.ApiServiceMocky
import com.jaspreet.editprofileapplication.repo.network.model.singlechoice.SingleChoiceValueResponse
import com.jaspreet.editprofileapplication.repo.pref.AppPrefrences
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SingleChoiceValuesUseCase  @Inject constructor
    (private val apiServiceMocky: ApiServiceMocky,
     private val appPreferences: AppPrefrences
     ){

    fun storeValueInPref()=
        apiServiceMocky.getSingleChoiceValues()
            .observeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())
            .map {
                 appPreferences.singleChoiceValues = Gson().toJson(it)
            }.subscribeOn(Schedulers.io())


    fun getSingleChoiceValueFromPref()=
       Single.fromCallable {
           appPreferences.singleChoiceValues
       }.subscribeOn(Schedulers.io())
           .observeOn(Schedulers.io())
           .map {
              return@map   Gson().fromJson(it,SingleChoiceValueResponse::class.java)
           }.subscribeOn(Schedulers.io())

}