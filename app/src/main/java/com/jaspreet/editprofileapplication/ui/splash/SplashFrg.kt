package com.jaspreet.editprofileapplication.ui.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jaspreet.editprofileapplication.R
import com.jaspreet.editprofileapplication.ui.base.BaseFragment
import javax.inject.Inject
import android.content.Intent
import com.jaspreet.editprofileapplication.ui.profile.EditProfileActivity
import java.util.*


class SplashFrg : BaseFragment<SplashViewModel>() {

    @Inject lateinit var splashViewModel: SplashViewModel

    override fun getViewModel(): SplashViewModel = splashViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.frg_splash,container,false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        splashViewModel.storeCities()
        splashViewModel.storeSingleValueChoices()

        Timer().schedule(object : TimerTask() {
           override fun run() {
                startActivity(Intent(requireContext(), EditProfileActivity::class.java))
                requireActivity().finish()
            }
        }, 4000)
    }
}

