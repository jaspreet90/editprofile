package com.jaspreet.editprofileapplication.ui.profile

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class EditProfileProvider{
@ContributesAndroidInjector(modules = [(EditProfileModule::class)])
abstract fun bindFrg(): EditProfileFrg
}