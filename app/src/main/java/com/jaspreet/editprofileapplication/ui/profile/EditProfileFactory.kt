package com.jaspreet.editprofileapplication.ui.profile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jaspreet.editprofileapplication.domain.CitiesUseCase
import com.jaspreet.editprofileapplication.domain.ProfileUseCase
import com.jaspreet.editprofileapplication.domain.SingleChoiceValuesUseCase
import javax.inject.Inject

class EditProfileFactory  @Inject constructor(
    private val profileUseCase: ProfileUseCase,
    private val citiesUseCase: CitiesUseCase,
    private val singleChoiceValuesUseCase: SingleChoiceValuesUseCase
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(EditProfileViewModel::class.java)) {

            return EditProfileViewModel(profileUseCase,citiesUseCase,singleChoiceValuesUseCase) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }


}