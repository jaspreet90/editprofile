package com.jaspreet.editprofileapplication.ui.splash

import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides


@Module
class SplashModule {
    @Provides
    fun provideViewModel(splashActivity: SplashActivity, splashFactory: SplashFactory) =
        ViewModelProvider(splashActivity, splashFactory).get(
            SplashViewModel::class.java
        )
}

