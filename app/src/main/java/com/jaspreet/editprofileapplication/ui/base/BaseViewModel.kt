package com.jaspreet.editprofileapplication.ui.base

 import androidx.lifecycle.MutableLiveData
 import androidx.lifecycle.ViewModel
 import com.google.android.gms.common.api.ApiException
 import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
 import com.jaspreet.editprofileapplication.R
 import com.jaspreet.editprofileapplication.util.EvaluationFailedException
 import io.reactivex.Completable
 import io.reactivex.Single
 import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
 import io.reactivex.exceptions.CompositeException
 import io.reactivex.plugins.RxJavaPlugins
 import java.net.UnknownHostException
 import javax.net.ssl.HttpsURLConnection

open class BaseViewModel : ViewModel() {
    private val errorLiveData: MutableLiveData<String> = MutableLiveData()
    private val internalErrorLiveData: MutableLiveData<Pair<Int, String?>> = MutableLiveData()
    private val blockingLoaderLiveData: MutableLiveData<Boolean> = MutableLiveData()
    private val successLiveData: MutableLiveData<Int> = MutableLiveData()
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    fun getInternalErrorObservable() = internalErrorLiveData
    fun getSuccessObservable() = successLiveData

    fun getBlockingLoaderObservable() = blockingLoaderLiveData


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    fun postError(messageResId: Int, extraText: String? = null) {
        internalErrorLiveData.postValue(Pair(messageResId, extraText))
    }

    fun postError(message: String?) {
        errorLiveData.postValue(message)
    }

    fun postSuccessToast(message: Int) {
        successLiveData.postValue(message)
    }


    private fun setBlockingLoading(isLoading: Boolean) {
        blockingLoaderLiveData.postValue(isLoading)
    }

    fun <T> Single<T>.applyLoader(): Single<T> =
        doOnSubscribe { setBlockingLoading(true) }
            .doFinally { setBlockingLoading(false) }

    fun Completable.applyLoader(): Completable =
        doOnSubscribe { setBlockingLoading(true) }
            .doFinally { setBlockingLoading(false) }

    fun handleExceptions(it: Throwable) {
        handleApiError(it)
        handleEvaluationException(it)
        RxJavaPlugins.onError(it)
    }

    fun handleApiError(throwable: Throwable) {
        RxJavaPlugins.onError(throwable)

        if (throwable is CompositeException) throwable.exceptions.forEach { handleHttpException(it) }
        else handleHttpException(throwable)
    }

    private fun handleHttpException(throwable: Throwable) {
        if (throwable is HttpException) {
            when (throwable.code()) {
                HttpsURLConnection.HTTP_FORBIDDEN -> {
                }
                HttpsURLConnection.HTTP_INTERNAL_ERROR ->
                    postError(R.string.something_went_wrong)
            }
            return
        }
        if (throwable is ApiException) {
            val message = throwable.message
            if (!message.isNullOrEmpty())
                postError(message)
        }
        if (throwable is UnknownHostException) {
            postError(R.string.no_internet_msg)
        }
    }
    fun handleEvaluationException(throwable: Throwable): Boolean {
        if (throwable is EvaluationFailedException) {
            return if (!throwable.message.isNullOrBlank()) {
                postError(throwable.message)
                true
            } else if (throwable.extraText == null) {
                postError(throwable.messageResId)
                true
            } else {
                postError(throwable.messageResId, throwable.extraText)
                true
            }
        }
        return false
    }



}