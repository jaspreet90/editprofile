package com.jaspreet.editprofileapplication.ui.splash

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class SplashProvider {
    @ContributesAndroidInjector(modules = [(SplashModule::class)])
    abstract fun bindFrg(): SplashFrg
}