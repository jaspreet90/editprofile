package com.jaspreet.editprofileapplication.ui.profile


import androidx.lifecycle.MutableLiveData
import com.jaspreet.editprofileapplication.R
import com.jaspreet.editprofileapplication.data.ProfileUI
import com.jaspreet.editprofileapplication.data.mapper.ProfileMapper.mapProfileResponseToUI
import com.jaspreet.editprofileapplication.domain.CitiesUseCase
import com.jaspreet.editprofileapplication.domain.ProfileUseCase
import com.jaspreet.editprofileapplication.domain.SingleChoiceValuesUseCase
import com.jaspreet.editprofileapplication.extensions.SELECT_PLACEHOLDER
import com.jaspreet.editprofileapplication.repo.db.location.LocationInfo
import com.jaspreet.editprofileapplication.repo.network.model.profile.ProfileResponse
import com.jaspreet.editprofileapplication.repo.network.model.singlechoice.*
import com.jaspreet.editprofileapplication.ui.base.BaseViewModel
import io.reactivex.schedulers.Schedulers
import io.reactivex.Single
import io.reactivex.functions.Function3

class EditProfileViewModel(private val profileUseCase: ProfileUseCase ,
                           private val citiesUseCase: CitiesUseCase ,
                           private val singleChoiceValuesUseCase: SingleChoiceValuesUseCase
                           ) : BaseViewModel() {

    private var userID="0"
    private var genderList = arrayListOf<String>()
    private var ethnicityList = arrayListOf<String>()
    private var religionList = arrayListOf<String>()
    private var figureList = arrayListOf<String>()
    private var maritalStatusList = arrayListOf<String>()
    private var cityList = arrayListOf<String>()

    private lateinit var singleChoiceValueResponse: SingleChoiceValueResponse
    private lateinit var cityListResponse: List<LocationInfo>
      val genderMutableLiveData=MutableLiveData<Pair<List<String>,Int>>()
      val ethnicityMutableLiveData=MutableLiveData<Pair<List<String>,Int>>()
      val religionMutableLiveData=MutableLiveData<Pair<List<String>,Int>>()
      val figureMutableLiveData=MutableLiveData<Pair<List<String>,Int>>()
      val maritalStatusMutableLiveData=MutableLiveData<Pair<List<String>,Int>>()
      val profileMutableLiveData=MutableLiveData<ProfileUI>()
      val cityLiveData = MutableLiveData<List<String>>()
        var lat:String? = null
        var lon:String? = null


    fun getCities(searchItem: String) {
        addDisposable( citiesUseCase.getCitiesFromDB(searchItem)
            .subscribe({
               cityListResponse = it
               cityList.clear()
                for (data in it){
                    cityList.add(data.cityName)
                }
                cityLiveData.postValue(cityList)
            },{
                handleExceptions(it)
            })
        )
    }


    fun setSelectedCity(index:Int){
         lat= cityListResponse[index].lat
         lon= cityListResponse[index].lon
    }

    fun  getProfile(){
        addDisposable(
            Single.zip(
            profileUseCase.getProfile(userID),
            singleChoiceValuesUseCase.getSingleChoiceValueFromPref(),
            citiesUseCase.getAllCitiesFromDB(),
            Function3< ProfileResponse,  SingleChoiceValueResponse,  List<LocationInfo>,   ProfileUI>    {
                    profilResponse,  singleChoiceValue, cities  ->

                var genderMatchIndex=0
                var ethnicityMatchIndex =0
                var religionMatchIndex =0
                var figureMatchIndex =0
                var maritalMatchIndex =0

                singleChoiceValueResponse = singleChoiceValue
                genderList.add(SELECT_PLACEHOLDER)


                for ((index,data) in singleChoiceValue.gender.withIndex() ){
                    if (profilResponse.gender == data.id){
                        genderMatchIndex = index
                        genderMatchIndex += 1
                    }
                    genderList.add(data.name)
                }

                ethnicityList.add(SELECT_PLACEHOLDER)

                for ((index,data) in singleChoiceValue.ethnicity.withIndex() ){
                    if (data.id == profilResponse.ethnicity){
                        ethnicityMatchIndex = index
                        ethnicityMatchIndex += 1
                    }
                    ethnicityList.add(data.name)
                }

                religionList.add(SELECT_PLACEHOLDER)
                for ((index,data) in singleChoiceValue.religion.withIndex() ){
                    if (data.id == profilResponse.religion) {
                        religionMatchIndex = index
                        religionMatchIndex += 1
                    }
                    religionList.add(data.name)
                }
                figureList.add(SELECT_PLACEHOLDER)
                for ((index,data) in singleChoiceValue.figure.withIndex() ){
                    if (data.id == profilResponse.figure){
                        figureMatchIndex = index
                        figureMatchIndex += 1
                    }
                    figureList.add(data.name)
                }
                maritalStatusList.add(SELECT_PLACEHOLDER)
                for ((index,data) in singleChoiceValue.maritalStatus.withIndex() ){
                    if (data.id== profilResponse.maritalStatus){
                        maritalMatchIndex = index
                        maritalMatchIndex += 1
                    }
                    maritalStatusList.add(data.name)
                }

                var cityName:String?=null
                for (data in cities) {
                        if (data?.lat == profilResponse?.lat && data?.lon == profilResponse?.lon) {
                             cityName = data.cityName
                             break
                        }
                }

                genderMutableLiveData.postValue(Pair(genderList,genderMatchIndex))
                ethnicityMutableLiveData.postValue(Pair(ethnicityList,ethnicityMatchIndex))
                religionMutableLiveData.postValue(Pair( religionList , religionMatchIndex))
                figureMutableLiveData.postValue(Pair( figureList,figureMatchIndex))
                maritalStatusMutableLiveData.postValue(Pair( maritalStatusList , maritalMatchIndex))

                return@Function3 mapProfileResponseToUI(profilResponse,cityName)
            }
        )
           .observeOn(Schedulers.io())
           .subscribeOn(Schedulers.io())
           .applyLoader()
           .subscribe({
               profileMutableLiveData.postValue(it)
           },{
                handleExceptions(it)
           })
        )
    }


    fun onClickEditProfile(
        displayName: String,
        realName: String,
        dob: String,
        genderIndex: Int,
        ethnicityIndex: Int,
        religionIndex: Int,
        figureIndex: Int,
        maritalStatusIndex: Int,
        occupation: String,
        aboutMe: String,
        height:String,
        location:String
    ) {
        var genderID:String?=null
        var maritalStatusID:String?=null

        if (ethnicityIndex==0)
            return
        if (religionIndex==0)
            return
        if (figureIndex==0)
            return



        if(genderIndex>0){
            genderID = singleChoiceValueResponse.gender[genderIndex-1].id
        }

        if(maritalStatusIndex>0){
            maritalStatusID = singleChoiceValueResponse.maritalStatus[maritalStatusIndex-1].id
        }


        addDisposable(
            profileUseCase.updateProfile(
                userID,
                displayName,
                realName,
                dob,
                genderID,
                singleChoiceValueResponse.ethnicity[ethnicityIndex-1].id,
                singleChoiceValueResponse.religion[religionIndex-1].id,
                singleChoiceValueResponse.figure[figureIndex-1].id,
                maritalStatusID,
                occupation,
                aboutMe,
                lat,
                lon,
                height
            )
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .applyLoader()
                .subscribe({
                 postSuccessToast(R.string.profile_updated_sucessfully)

                },{
                    handleExceptions(it)
                })
        )
    }
}