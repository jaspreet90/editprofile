package com.jaspreet.editprofileapplication.ui.profile

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.jaspreet.editprofileapplication.R
import com.jaspreet.editprofileapplication.extensions.onItemSelected
import com.jaspreet.editprofileapplication.ui.base.BaseFragment
import com.jaspreet.editprofileapplication.util.DefaultSpinnerAdapter
import kotlinx.android.synthetic.main.custom_spinner.view.*
import kotlinx.android.synthetic.main.frg_edit_profile.*
import javax.inject.Inject
import android.widget.ArrayAdapter
import com.jaspreet.editprofileapplication.data.ProfileUI
import android.app.DatePickerDialog
import java.util.*


class EditProfileFrg :BaseFragment<EditProfileViewModel>() {

    @Inject lateinit var editProfileViewModel: EditProfileViewModel

    var genderIndex=0
    var ethnicityIndex=0
    var religionIndex=0
    var figureIndex=0
    var maritalStatusIndex=0
    var cityList = arrayListOf<String>()
    lateinit var cityAdapter:ArrayAdapter<String>

    override fun getViewModel(): EditProfileViewModel = editProfileViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =  inflater.inflate(R.layout.frg_edit_profile,container,false)



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        editProfileViewModel.getProfile()
        bindViewModel()
        setClickListener()
        setSpinnerSelection()
        setAutoCompleteText()
    }
    private fun setAutoCompleteText(){
        act_location.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                var searchItem =s.toString()
                editProfileViewModel.getCities(searchItem)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
             }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

             }
        })

         act_location.setOnItemClickListener { parent, view, position, id ->
             editProfileViewModel.setSelectedCity(position)
         }

        cityAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, cityList)
        act_location.setAdapter(cityAdapter)


    }

    private fun setUpTextField(it: ProfileUI) {
        et_display_name.setText( it.displayName )
        et_real_name.setText( it.realName )
        et_occupation.setText( it.occupation )
        et_about_me.setText( it.aboutMe )
        et_height.setText( it.height )
        act_location.setText( it.city )
        tv_dob.setText( it.birthday )
    }

    private fun bindViewModel(){
        editProfileViewModel.maritalStatusMutableLiveData.observe(this, Observer {
            fl_marital_staus.spinner.adapter = DefaultSpinnerAdapter(requireContext(), it.first,-1)
            fl_marital_staus.spinner.setSelection(it.second)
        })

        editProfileViewModel.religionMutableLiveData.observe(this, Observer {
            fl_religion.spinner.adapter = DefaultSpinnerAdapter(requireContext(), it.first,0)
            fl_religion.spinner.setSelection(it.second)
        })

        editProfileViewModel.ethnicityMutableLiveData.observe(this, Observer {
            fl_ethinicity.spinner.adapter = DefaultSpinnerAdapter(requireContext(), it.first,0)
            fl_ethinicity.spinner.setSelection(it.second)
        })

        editProfileViewModel.figureMutableLiveData.observe(this, Observer {
            fl_figure.spinner.adapter = DefaultSpinnerAdapter(requireContext(), it.first,0)
            fl_figure.spinner.setSelection(it.second)
        })

        editProfileViewModel.genderMutableLiveData.observe(this, Observer {
            fl_gender.spinner.adapter = DefaultSpinnerAdapter(requireContext(), it.first,-1)
            fl_gender.spinner.setSelection(it.second)
        })

        editProfileViewModel.profileMutableLiveData.observe(this, Observer {
            setUpTextField(it)
        })

        editProfileViewModel.cityLiveData.observe(this, Observer {
            cityList.clear()
            cityList.addAll(it)
            cityAdapter.clear()
            cityAdapter.addAll(cityList)
        })
    }



    private fun setSpinnerSelection(){
        fl_gender.spinner.onItemSelected { _, _, position, _ ->
            genderIndex =position
        }
        fl_ethinicity.spinner.onItemSelected { _, _, position, _ ->
            ethnicityIndex =position
        }
        fl_figure.spinner.onItemSelected { _, _, position, _ ->
            figureIndex =position
        }
        fl_marital_staus.spinner.onItemSelected { _, _, position, _ ->
            maritalStatusIndex =position
        }
        fl_religion.spinner.onItemSelected { _, _, position, _ ->
            religionIndex =position
        }
    }

    private fun setClickListener(){
        btn_edit.setOnClickListener {
            editProfileViewModel.onClickEditProfile(
               displayName =  et_display_name.text.toString(),
               realName =  et_real_name.text.toString(),
               dob =  tv_dob.text.toString(),
               genderIndex =  genderIndex,
               ethnicityIndex =  ethnicityIndex,
               religionIndex =  religionIndex,
               figureIndex =  figureIndex,
               maritalStatusIndex =  maritalStatusIndex,
               occupation =  et_occupation.text.toString(),
               aboutMe =  et_about_me.text.toString(),
                height = et_height.text.toString(),
                location = act_location.text.toString()
               )
        }

        iv_calendar.setOnClickListener {
            openDatePicker()
        }
    }

    fun openDatePicker(){
        val c = Calendar.getInstance()
        var mYear = c.get(Calendar.YEAR)
        var  mMonth = c.get(Calendar.MONTH)
        var  mDay = c.get(Calendar.DAY_OF_MONTH)


        val datePickerDialog = DatePickerDialog(requireContext(),
            DatePickerDialog.OnDateSetListener {
                    view, year, monthOfYear, dayOfMonth ->
                   tv_dob.setText(dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
            },
            mYear,
            mMonth,
            mDay
        )
        datePickerDialog.show()
    }
}