package com.jaspreet.editprofileapplication.ui.profile

import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides

@Module
class EditProfileModule {
    @Provides
    fun provideViewModel(editProfileActivity: EditProfileActivity, editProfileFactory: EditProfileFactory) =
        ViewModelProvider(editProfileActivity, editProfileFactory).get(
            EditProfileViewModel::class.java
        )
}

