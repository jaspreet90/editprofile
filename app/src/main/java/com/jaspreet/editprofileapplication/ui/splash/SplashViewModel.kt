package com.jaspreet.editprofileapplication.ui.splash

import android.util.Log
import com.jaspreet.editprofileapplication.domain.CitiesUseCase
import com.jaspreet.editprofileapplication.domain.SingleChoiceValuesUseCase
import com.jaspreet.editprofileapplication.ui.base.BaseViewModel
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class SplashViewModel(
    private val citiesUseCase: CitiesUseCase,   private val singleChoiceValuesUseCase: SingleChoiceValuesUseCase
) : BaseViewModel() {

    private var cityFileName="cities.json"


    fun storeCities(){

        Log.d("==store cities","==store")
       addDisposable(  citiesUseCase.storeLocationIntoDB(cityFileName)
            .subscribeOn(Schedulers.io())
            .subscribe({
                Log.d("==store cities res=","==store res")
            },{
                Log.d("==store cities err=$it" ,"==store err")

        })
       )
    }


    fun storeSingleValueChoices(){
        addDisposable(
            singleChoiceValuesUseCase
                .storeValueInPref()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe({
                    Log.d("==res== single value","==$it")
                },{
                    Log.d("==err= single value","=eer=$it")
                })
        )
    }


}