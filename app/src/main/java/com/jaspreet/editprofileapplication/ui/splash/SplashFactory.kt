package com.jaspreet.editprofileapplication.ui.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jaspreet.editprofileapplication.domain.CitiesUseCase
import com.jaspreet.editprofileapplication.domain.SingleChoiceValuesUseCase
import javax.inject.Inject

class SplashFactory @Inject constructor(
    private val citiesUseCase: CitiesUseCase,
    private val singleChoiceValuesUseCase: SingleChoiceValuesUseCase
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SplashViewModel::class.java)) {

            return SplashViewModel(citiesUseCase, singleChoiceValuesUseCase) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }


}