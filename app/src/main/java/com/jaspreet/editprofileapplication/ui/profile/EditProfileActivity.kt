package com.jaspreet.editprofileapplication.ui.profile

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.jaspreet.editprofileapplication.R
import com.jaspreet.editprofileapplication.ui.base.BaseActivity
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class EditProfileActivity : BaseActivity() , HasSupportFragmentInjector {

    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>


    override  fun supportFragmentInjector(): AndroidInjector<Fragment>?  =   fragmentDispatchingAndroidInjector



    override
    fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_common)
        addOrReplaceFrg(R.id.fl_container, EditProfileFrg(),false,false)
    }
}