package com.jaspreet.editprofileapplication.ui.base

import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.jaspreet.editprofileapplication.extensions.longToast
import com.jaspreet.editprofileapplication.extensions.showLoadingDialog
import com.jaspreet.editprofileapplication.extensions.toast
import dagger.android.support.AndroidSupportInjection
import timber.log.Timber

abstract class BaseFragment<out VM : BaseViewModel> : Fragment() {

    private var mProgressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (hasInjector()) {
            AndroidSupportInjection.inject(this)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getViewModel()?.getInternalErrorObservable()?.observe(viewLifecycleOwner, Observer {
            it?.let {
                val message = if (it.second == null) getString(it.first) else getString(it.first, it.second)
                activity?.longToast(message)
            }
        })

        getViewModel()?.getBlockingLoaderObservable()?.observe(viewLifecycleOwner, Observer {
            it?.let {
                  toggleLoaderVisibility(it)
            }
        })

        getViewModel().getSuccessObservable().observe(this, Observer {
            it?.let {

                 toast(resources.getString(it))
            }
        })
    }

    fun toggleLoaderVisibility(visibility: Boolean) {
        if (visibility) showLoading() else hideLoading()
    }

    private fun showLoading() {
         hideLoading()
        mProgressDialog = showLoadingDialog()
    }

    private fun hideLoading() {
        Timber.d(" hide loading")
        mProgressDialog?.let {
            if (it.isShowing)
                it.cancel()
        }
        mProgressDialog = null
    }



    open fun hasInjector() = true

    abstract fun getViewModel(): VM
}