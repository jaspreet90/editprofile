package com.jaspreet.editprofileapplication.extensions

 import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
 import android.util.Log
 import android.widget.Toast
import androidx.fragment.app.Fragment
import com.jaspreet.editprofileapplication.R

fun Context.longToast(message: String?) {
    if (!message.isNullOrBlank())
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun Fragment.showLoadingDialog(): ProgressDialog {
    val progressDialog = ProgressDialog(requireContext())
    progressDialog.show()
    progressDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    progressDialog.setContentView(R.layout.progress_dialog)
    progressDialog.isIndeterminate = true
    progressDialog.setCancelable(false)
    progressDialog.setCanceledOnTouchOutside(false)
    return progressDialog
}

fun Fragment.toast(message: String?) {

    Log.d("==msg==","==$message")

    if (!message.isNullOrBlank())
        android.widget.Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
}

