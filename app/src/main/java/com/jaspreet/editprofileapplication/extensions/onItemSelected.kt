package com.jaspreet.editprofileapplication.extensions
import android.view.View
import android.widget.AdapterView
import android.widget.Spinner

inline fun Spinner.onItemSelected(crossinline onItemSelected: (AdapterView<*>, View?, Int, Long) -> Unit) {
    onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {

        }

        override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
            onItemSelected.invoke(parent, view, position, id)
        }
    }
}

fun Spinner.getSelectedTrimmedItem() = if (selectedItem == null) "" else selectedItem.toString().trim()