package com.jaspreet.editprofileapplication.di.component

import com.jaspreet.editprofileapplication.EditProfileApplication
import com.jaspreet.editprofileapplication.di.builder.ActivityBuilder
import com.jaspreet.editprofileapplication.di.module.ApplicationModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton


@Singleton
@Component(modules = [(AndroidInjectionModule::class), (ApplicationModule::class), (ActivityBuilder::class) ])
interface ApplicationComponent {

    fun inject(app: EditProfileApplication)

}