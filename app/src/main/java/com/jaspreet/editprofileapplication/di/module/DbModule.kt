package com.jaspreet.editprofileapplication.di.module

import android.app.Application
import com.jaspreet.editprofileapplication.repo.db.AppDatabase
import com.jaspreet.editprofileapplication.repo.db.location.LocationImpl
import com.jaspreet.editprofileapplication.repo.db.location.LocationRepo
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DbModule {

    @Provides
    @Singleton
    internal fun provideAppDb(application: Application) = AppDatabase.createInstance(application)

    @Provides
    @Singleton
    internal fun provideCelebrityDao(appDb: AppDatabase) = appDb.locationDao()


    @Singleton
    @Provides
    internal fun productRepository(locationImpl: LocationImpl): LocationRepo = locationImpl
}