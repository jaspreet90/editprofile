package com.jaspreet.editprofileapplication.di.builder

import com.jaspreet.editprofileapplication.ui.profile.EditProfileActivity
import com.jaspreet.editprofileapplication.ui.profile.EditProfileProvider
import com.jaspreet.editprofileapplication.ui.splash.SplashActivity
import com.jaspreet.editprofileapplication.ui.splash.SplashProvider
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuilder {
    @ContributesAndroidInjector(modules = [EditProfileProvider::class])
    abstract fun bindEditProfile() : EditProfileActivity

    @ContributesAndroidInjector(modules = [SplashProvider::class])
    abstract fun bindSplash() : SplashActivity
}