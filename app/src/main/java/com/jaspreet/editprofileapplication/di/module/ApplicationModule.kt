package com.jaspreet.editprofileapplication.di.module

import android.app.Application
import android.content.Context
import com.jaspreet.editprofileapplication.readassets.ReadAsset
import com.jaspreet.editprofileapplication.readassets.ReadAssetsImpl
import com.jaspreet.editprofileapplication.repo.pref.AppPrefrences
import com.jaspreet.editprofileapplication.repo.pref.AppPrefrencesImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module(includes = [(NetworkModule::class) , (DbModule::class)])
class ApplicationModule (val app: Application){

    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun provideContext(): Context =app

    @Provides
    @Singleton
    fun provideReadAsset(readAssetsImpl: ReadAssetsImpl) : ReadAsset = readAssetsImpl

    @Provides
    @Singleton
    internal fun providePreferencesHelper(  appPrefrencesImpl: AppPrefrencesImpl ): AppPrefrences = appPrefrencesImpl
}