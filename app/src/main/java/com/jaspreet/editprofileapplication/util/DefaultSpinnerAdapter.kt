package com.jaspreet.editprofileapplication.util

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.jaspreet.editprofileapplication.R

class DefaultSpinnerAdapter(context: Context, list: List<String>,private val pos:Int) : ArrayAdapter<String>(context, R.layout.support_simple_spinner_dropdown_item, list) {

    override fun isEnabled(position: Int) : Boolean{

      return  position != pos


    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = super.getView(position, convertView, parent)
        colorTextByPosition(position, view)
        return view
    }

    override fun getDropDownView(position: Int, convertView: View?,
                                 parent: ViewGroup): View {
        val view = super.getDropDownView(position, convertView, parent)
        colorTextByPosition(position, view)
        return view
    }

    private fun colorTextByPosition(position: Int, view: View) {
        if (view is TextView) {
            view.setTextColor(if (position == 0) Color.GRAY else Color.BLACK)
        }
    }
}