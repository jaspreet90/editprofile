package com.jaspreet.editprofileapplication.readassets

import io.reactivex.Flowable

interface ReadAsset {
    fun read(fileName :String) : Flowable<String>
}