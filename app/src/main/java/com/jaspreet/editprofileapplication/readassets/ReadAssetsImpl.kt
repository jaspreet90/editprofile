package com.jaspreet.editprofileapplication.readassets

import android.content.Context
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReadAssetsImpl @Inject constructor(private  val context: Context) : ReadAsset {
    override fun read(fileName:String) : Flowable<String> {
        var inputStream= context.assets.open(fileName)

        return  Flowable.fromCallable {
            return@fromCallable  inputStream.bufferedReader().use { it.readText() }
        }.observeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())

    }
}