package com.jaspreet.editprofileapplication.data

import com.google.gson.annotations.SerializedName

data class ProfileUI (


    @SerializedName("aboutMe")
    val aboutMe: String,
    @SerializedName("dob")
    val birthday: String,
    @SerializedName("displayName")
    val displayName: String,
    @SerializedName("occupation")
    val occupation: String,
    @SerializedName("realName")
    val realName: String,
    @SerializedName("height")
    val height: String,
    @SerializedName("city")
    val city: String

)