package com.jaspreet.editprofileapplication.data.mapper

import com.jaspreet.editprofileapplication.data.ProfileUI
import com.jaspreet.editprofileapplication.repo.network.model.profile.ProfileResponse

object ProfileMapper {

    fun mapProfileResponseToUI(profilResponse: ProfileResponse, city: String?) =
        ProfileUI(
            aboutMe = profilResponse.aboutMe,
            occupation = profilResponse.occupation,
            realName = profilResponse.realName,
            displayName = profilResponse.displayName,
            birthday = profilResponse.birthday,
            city = city ?: "",
            height = profilResponse.height
        )
}