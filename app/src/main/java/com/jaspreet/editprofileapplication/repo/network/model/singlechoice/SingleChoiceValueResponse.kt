package com.jaspreet.editprofileapplication.repo.network.model.singlechoice


import com.google.gson.annotations.SerializedName

data class SingleChoiceValueResponse(
    @SerializedName("ethnicity")
    val ethnicity: List<Ethnicity>,
    @SerializedName("figure")
    val figure: List<Figure>,
    @SerializedName("gender")
    val gender: List<Gender>,
    @SerializedName("marital_status")
    val maritalStatus: List<MaritalStatu>,
    @SerializedName("religion")
    val religion: List<Religion>
)