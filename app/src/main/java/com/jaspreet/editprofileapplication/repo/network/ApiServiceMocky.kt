package com.jaspreet.editprofileapplication.repo.network

import com.jaspreet.editprofileapplication.repo.network.model.singlechoice.SingleChoiceValueResponse
import io.reactivex.Flowable
import io.reactivex.Single
import retrofit2.http.GET
import javax.inject.Named
import javax.inject.Singleton

@Singleton
interface ApiServiceMocky {

    @GET("v2/5d45eea03000002f66c5c976")
    fun getSingleChoiceValues() : Single<SingleChoiceValueResponse>


}