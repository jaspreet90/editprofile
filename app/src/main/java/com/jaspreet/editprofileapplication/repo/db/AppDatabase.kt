package com.jaspreet.editprofileapplication.repo.db

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.jaspreet.editprofileapplication.repo.db.location.LocationDao
import com.jaspreet.editprofileapplication.repo.db.location.LocationInfo

@Database(entities = [LocationInfo::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun locationDao() : LocationDao
    companion object {
        private val DB_NAME = "AppDatabase.db"

        fun createInstance(application: Application): AppDatabase {
            return Room.databaseBuilder(application, AppDatabase::class.java, DB_NAME)
                .build()
        }

    }
}