package com.jaspreet.editprofileapplication.repo.network

import javax.inject.Inject

class ApiRepositoryMockyImpl @Inject
constructor( private val apiServiceMocky: ApiServiceMocky )  : ApiRepositorymocky {

    override fun getSingleChoiceValues() = apiServiceMocky.getSingleChoiceValues()
}