package com.jaspreet.editprofileapplication.repo.network.model.location


import com.google.gson.annotations.SerializedName

data class City(
    @SerializedName("city")
    val city: String,
    @SerializedName("lat")
    val lat: String,
    @SerializedName("lon")
    val lon: String
)