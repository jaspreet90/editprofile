package com.jaspreet.editprofileapplication.repo.network

import com.jaspreet.editprofileapplication.repo.network.model.profile.ProfileResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.PUT



interface ApiService  {

    @GET("user/{id}.json")
    fun getProfile(@Path("id") id:String  ) : Single<ProfileResponse>

    @PUT("user/{id}.json")
    fun updateProfile(@Path("id") id:String, @Body profileBody: ProfileResponse): Single<ProfileResponse>
}