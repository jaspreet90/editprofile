package com.jaspreet.editprofileapplication.repo.db.location

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface LocationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(locationInfo: List<LocationInfo>)

    @Query("SELECT * from LOCATION_TABLE where CITY_NAME like :searchItem limit 10")
    fun getSearchedLocations(searchItem: String): Flowable<List<LocationInfo>>

    @Query("SELECT * from LOCATION_TABLE")
    fun getAllCities(): Single<List<LocationInfo>>
}