package com.jaspreet.editprofileapplication.repo.network.model.singlechoice


import com.google.gson.annotations.SerializedName

data class MaritalStatu(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String
)