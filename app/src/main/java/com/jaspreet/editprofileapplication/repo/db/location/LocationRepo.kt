package com.jaspreet.editprofileapplication.repo.db.location

import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface LocationRepo {

    fun insert(locationInfo: List<LocationInfo>) : Completable

    fun getSearchedLocations(searchItem: String): Flowable<List<LocationInfo>>

    fun getAllCities(): Single<List<LocationInfo>>
}