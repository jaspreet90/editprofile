package com.jaspreet.editprofileapplication.repo.network.model.location


import com.google.gson.annotations.SerializedName

data class LocationReponse(
    @SerializedName("cities")
    val cities: List<City>
)