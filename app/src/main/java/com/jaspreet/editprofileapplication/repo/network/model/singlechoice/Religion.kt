package com.jaspreet.editprofileapplication.repo.network.model.singlechoice


import com.google.gson.annotations.SerializedName

data class Religion(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String
)