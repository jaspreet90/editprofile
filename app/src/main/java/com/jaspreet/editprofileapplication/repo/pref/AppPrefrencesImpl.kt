package com.jaspreet.editprofileapplication.repo.pref

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppPrefrencesImpl@Inject constructor(context: Application) : AppPrefrences {

   private val mPrefs: SharedPreferences = context.getSharedPreferences("App_prefs", Context.MODE_PRIVATE)


    override var singleChoiceValues: String
        get() = mPrefs.getString(PREF_KEY_SAVE_SINGLE_VALUES, null)
        set(values) = mPrefs.edit().putString(PREF_KEY_SAVE_SINGLE_VALUES, values).apply()

    companion object {
        private const val PREF_KEY_SAVE_SINGLE_VALUES = "PREF_KEY_SAVE_SINGLE_VALUES"
    }
}