package com.jaspreet.editprofileapplication.repo.db.location

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class LocationImpl @Inject constructor(private val locationDao: LocationDao) :
    LocationRepo {

    override fun insert(locationInfo: List<LocationInfo>): Completable = Completable.fromAction {
        locationDao.insert(locationInfo)
    }

    override fun getSearchedLocations(searchItem:String): Flowable<List<LocationInfo>> = locationDao.getSearchedLocations(searchItem)

    override fun getAllCities(): Single<List<LocationInfo>> {
         return locationDao.getAllCities()
    }
}