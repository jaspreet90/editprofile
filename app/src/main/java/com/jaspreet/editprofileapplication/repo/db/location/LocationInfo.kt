package com.jaspreet.editprofileapplication.repo.db.location

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "LOCATION_TABLE", primaryKeys = ["LAT", "LON"])
data class LocationInfo (

    @ColumnInfo(name="CITY_NAME")
    @NonNull
    var  cityName : String,

    @ColumnInfo(name="LAT")
    @NonNull
    var lat :String,

    @ColumnInfo(name="LON")
    @NonNull
    var lon : String
)