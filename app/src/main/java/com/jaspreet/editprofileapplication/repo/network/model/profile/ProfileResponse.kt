package com.jaspreet.editprofileapplication.repo.network.model.profile


import com.google.gson.annotations.SerializedName

data class ProfileResponse(
    @SerializedName("aboutMe")
    val aboutMe: String,
    @SerializedName("dob")
    val birthday: String,
    @SerializedName("displayName")
    val displayName: String,
    @SerializedName("ethnicity")
    val ethnicity: String,
    @SerializedName("figure")
    val figure: String,
    @SerializedName("gender")
    val gender: String?,
    @SerializedName("height")
    val height: String,
    @SerializedName("lat")
    val lat: String,
    @SerializedName("lon")
    val lon: String,
    @SerializedName("maritalStatus")
    val maritalStatus: String?,
    @SerializedName("occupation")
    val occupation: String,
    @SerializedName("realName")
    val realName: String,
    @SerializedName("religion")
    val religion: String?
)