package com.jaspreet.editprofileapplication.repo.network

import dagger.Provides
import io.reactivex.Single
import retrofit2.http.GET
import javax.inject.Named
import javax.inject.Singleton

@Singleton
interface ApiRepository: ApiService  {
}
