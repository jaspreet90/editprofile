package com.jaspreet.editprofileapplication.repo.network

import com.jaspreet.editprofileapplication.repo.network.model.profile.ProfileResponse
import io.reactivex.Single
import javax.inject.Inject

class ApiRepositoryImpl
@Inject
constructor( private val apiService: ApiService ) : ApiRepository {
    override fun updateProfile(id: String, profileBody: ProfileResponse): Single<ProfileResponse> =
        apiService.updateProfile(id , profileBody)


    override fun getProfile(id:String) :Single<ProfileResponse> = apiService.getProfile(id)
}