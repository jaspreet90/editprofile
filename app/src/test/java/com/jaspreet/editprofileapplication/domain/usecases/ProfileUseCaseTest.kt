package com.jaspreet.editprofileapplication.domain.usecases

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jaspreet.editprofileapplication.domain.ProfileUseCase
import com.jaspreet.editprofileapplication.domain.common.profileResponse
import com.jaspreet.editprofileapplication.domain.common.whenever
import com.jaspreet.editprofileapplication.repo.network.ApiRepository
import io.reactivex.Single
import org.junit.After
import org.junit.Rule
import org.junit.Test
import org.mockito.MockitoAnnotations
import org.junit.Before
import org.mockito.Mock
import org.mockito.Mockito
import java.util.*


class ProfileUseCaseTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var apiRepository:ApiRepository

    private lateinit var profileUseCases:  ProfileUseCase

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        profileUseCases = ProfileUseCase(apiRepository)
    }

    @After
    fun tearDown() {
        Mockito.reset(apiRepository)
    }

    @Test
    fun testGetProfile_response() {

        var ID="0"
         whenever(apiRepository.getProfile(ID))
            .thenReturn(Single.just(profileResponse()))

        profileUseCases.getProfile(ID)
            .test()
            .await().assertComplete()
    }

    @Test
    fun testGetProfile_error() {

        val num = 10000 // Suppose ids are upto ten thousand
        val rand = Random()
        var ran = rand.nextInt(num) +1

        val response = Throwable("Error response")

        var ID=ran.toString()
        whenever(apiRepository.getProfile(ID))
            .thenReturn(Single.error(response))

        profileUseCases.getProfile(ID)
            .test()
             .await()
             .assertError(response)
    }
}