package com.jaspreet.editprofileapplication.domain.usecases

import com.jaspreet.editprofileapplication.domain.CitiesUseCase
import com.jaspreet.editprofileapplication.repo.db.location.LocationRepo
import io.reactivex.Single
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import com.jaspreet.editprofileapplication.domain.common.locationInfo
import com.jaspreet.editprofileapplication.domain.common.whenever
import io.reactivex.Flowable
import org.mockito.ArgumentMatchers.anyString
import java.io.IOException

class CitiesUseCaseTest {

    private lateinit var citiesUseCase: CitiesUseCase
    @Mock
    private lateinit var locationRepo: LocationRepo

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        citiesUseCase = CitiesUseCase(locationRepo)
    }


    @After
    fun tearDown() {
        Mockito.reset(locationRepo)
    }


    @Test
    fun getCitiesFromDB_success() {

        var searchItem=anyString()
        val response = locationInfo()
        whenever(locationRepo.getSearchedLocations("%$searchItem%"))
            .thenReturn(Flowable.just(response))

        citiesUseCase.getCitiesFromDB(searchItem)
            .test()
            .await().assertComplete()

    }

    @Test
    fun getCitiesFromDB_fail() {

        var searchItem=anyString()
        val response = IOException()
        whenever(locationRepo.getSearchedLocations("%$searchItem%"))
            .thenReturn(Flowable.error(response))

        citiesUseCase.getCitiesFromDB(searchItem)
            .test()
            .await()
            .assertError(response)

    }

    @Test
    fun getAllCities_sucess() {

        val response = locationInfo()
        whenever(locationRepo.getAllCities())
            .thenReturn(Single.just(response))

        citiesUseCase.getAllCitiesFromDB()
            .test()
            .await().assertComplete()
    }

    @Test
    fun getAllCities_fail() {

        val response = IOException()
       whenever(locationRepo.getAllCities())
            .thenReturn(Single.error(response))

       citiesUseCase.getAllCitiesFromDB()
            .test()
            .await()
            .assertError(response)
    }
}