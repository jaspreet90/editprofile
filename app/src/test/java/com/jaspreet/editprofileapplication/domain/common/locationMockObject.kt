package com.jaspreet.editprofileapplication.domain.common

import com.jaspreet.editprofileapplication.repo.db.location.LocationInfo


fun locationInfoList() :  List<LocationInfo>{
    var locationInfoList= arrayListOf<LocationInfo>()
    locationInfoList.add(LocationInfo("Aarhus","56°09'N","10°13'E"))
    locationInfoList.add(LocationInfo("Aberdeen","57°09'N" ,"2°07'W"))
    locationInfoList.add(LocationInfo("Abidjan","5°19'N" ,"4°02'W"))

    return locationInfoList
}

fun locationInfo() =  locationInfoList()