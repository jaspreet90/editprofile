package com.jaspreet.editprofileapplication.domain.common

import com.jaspreet.editprofileapplication.repo.network.model.singlechoice.*


fun genderList() : ArrayList<Gender>{
    var genderList= arrayListOf<Gender>()
    genderList.add(Gender("8f9d76ad-2c6b-4a98-8496-6165a2770a5e","Male"))
    genderList.add(Gender("1969cf48-7ae7-4073-abb3-d09ba6a19946","FeMale"))
    genderList.add(Gender("43a01b78-5425-4cba-821a-1459dd5a2784","Other"))

    return genderList
}

fun ethnicityList() : ArrayList<Ethnicity>{
    var ethnicityList= arrayListOf<Ethnicity>()
    ethnicityList.add(Ethnicity("5b3d1252-860f-459b-ab90-7a2914360dbf","White"))
    ethnicityList.add(Ethnicity("1b2f380e-5d70-4ada-9ad3-c6d733a1aaa4","South Asian"))
    ethnicityList.add(Ethnicity("a55f5912-8a13-4c0e-bf11-d333f93a08e0","South East Asian"))
    ethnicityList.add(Ethnicity("44d17f32-f0c9-4bd0-801e-fd9b63d54342","Mixed"))

    return ethnicityList
}

fun religionList() : ArrayList<Religion>{
    var religionList= arrayListOf<Religion>()
    religionList.add(Religion("a2bc1142-9b6a-41f3-a620-a39afb1304ab","Agnostic"))
    religionList.add(Religion("2601e45e-b70f-43e3-8364-205794b439e6","Atheist"))
    religionList.add(Religion("aec22ee3-fd02-4570-89bd-16310c43dc31","Buddhist"))

    return religionList
}

fun figureList() : ArrayList<Figure>{
    var figureList= arrayListOf<Figure>()
    figureList.add(Figure("9c6ddf44-01ae-4fdb-acc9-b97f2882e4ef","Slim"))
    figureList.add(Figure("b5e4720e-bc9a-4e56-a301-31a6f667adc2","Normal"))

    return figureList
}

fun maritalStatusList() : ArrayList<MaritalStatu>{
    var maritalStatusList= arrayListOf<MaritalStatu>()
    maritalStatusList.add(MaritalStatu("5a837767-7a11-487c-a243-7451c7b14c03","Never Married"))
    maritalStatusList.add(MaritalStatu("1a02c6a4-d7b1-470c-839d-8c3a099a9665","Divorced"))

    return maritalStatusList
}

fun singleChoiceValueResponse() = SingleChoiceValueResponse(
        ethnicity =  ethnicityList(),
        figure =  figureList(),
        gender =  genderList(),
        maritalStatus = maritalStatusList(),
        religion = religionList()
    )
