package com.jaspreet.editprofileapplication.domain.common

import com.google.gson.annotations.SerializedName
import com.jaspreet.editprofileapplication.repo.network.model.profile.ProfileResponse
import com.jaspreet.editprofileapplication.repo.network.model.singlechoice.*

fun profileResponse() =
    ProfileResponse("I am a Software Engineer", "09-01-1990", "Jaspreet", "", "", "", "5 feet 7 inch", "", "", "", "S.E", "Jaspreet Singh", "")

