package com.jaspreet.editprofileapplication.domain.usecases


import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jaspreet.editprofileapplication.domain.SingleChoiceValuesUseCase
import com.jaspreet.editprofileapplication.domain.common.singleChoiceValueResponse
import com.jaspreet.editprofileapplication.domain.common.whenever
import com.jaspreet.editprofileapplication.repo.network.ApiServiceMocky
import com.jaspreet.editprofileapplication.repo.pref.AppPrefrences
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.io.IOException

class SingleChoiceValueUseCaseTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var apiServiceMocky:ApiServiceMocky

    @Mock
    private lateinit var appPrefrences:AppPrefrences

    private lateinit var singleChoiceValuesUseCase: SingleChoiceValuesUseCase

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        singleChoiceValuesUseCase= SingleChoiceValuesUseCase(apiServiceMocky, appPrefrences)
    }

    @Test
    fun testGetSingleChoiceValueFromPref_sucess(){

        whenever(apiServiceMocky.getSingleChoiceValues())
            .thenReturn(Single.just(singleChoiceValueResponse()))

        singleChoiceValuesUseCase.getSingleChoiceValueFromPref()
            .test()
            .awaitTerminalEvent()

    }

    @Test
    fun testGetSingleChoiceValueFromPref_fail(){

        var ioException =IOException()

        whenever(apiServiceMocky.getSingleChoiceValues())
            .thenReturn(Single.error(ioException))

        singleChoiceValuesUseCase.getSingleChoiceValueFromPref()
            .test()
            .awaitTerminalEvent()
    }


}